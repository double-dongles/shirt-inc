import {promises as fs} from 'fs';
import path from 'path';

import Layout from '../components/layout';
import Product from '../components/product';

export default function Home({products}) {
  return <Layout>
    {products.map((d, id) => {
      return <Product d={d} key={id} />;
    })}
  </Layout >;
}

export async function getStaticProps() {
  const databaseFile = path.join(process.cwd(), 'database', 'items.json');
  const data = await fs.readFile(databaseFile, 'utf-8');
  const products = JSON.parse(data);

  return {
    props: {
      products,
    },
  };
}
