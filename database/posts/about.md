# About page

## About the company

Our company, Shirt Inc, is founded to be one of the most important in terms of asset
management as well to be extremely precise in our T shirt making process. 

We are currently offering two products *at the time of writing*:
- A hoodie
- A T shirt
- A shoes

We very much had perfected the art of calibrating, as in T shirt calibration for each and every customer's 
size, so much as to do it by predicting the future.

## Why no uniform

When I formed this company, I've decided that uniform is that ultimate factor that would limit the creativity that a person would have. 

And so there would be no uniforms for each employees, and they are free to wear anything they like.
As long as these are decent then there would be no restrictions. Practically we allow every single
kind of clothing there are.

## About the CEO

Hi! I'm Dang, currently the most important person in this componany. I'm the CEO, CTO, CLO, CCO,
COO, CDO, CHRM, CIO, CSO,... and the **sole** developer working on this website. 

![Dang](/avatar.jpg)

You can find me here!
- [Codeberg](https://codeberg.org/double-dongles)
- [Reddit](https://www.reddit.com/user/ICantTypeMyUsername)
- [Or just email me!](mailto:double-dongles@tutanota.com)
