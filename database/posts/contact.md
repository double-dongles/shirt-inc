# Contacts

Call: 0969 420 420

Address: Thu Thiem

- [Youtube](https://www.youtube.com/channel/UCC6v1OIw3aJAwOJjkJlUEJw)
- [Facebook](https://www.facebook.com/doubledongles)
- [Codeberg](https://codeberg.org/double-dongles)
- [Reddit](https://www.reddit.com/user/ICantTypeMyUsername)
- [Or just email me!](mailto:double-dongles@tutanota.com)
