import Layout from '../components/layout';
import { serialize } from 'next-mdx-remote/serialize';
import { MDXRemote } from 'next-mdx-remote';
import { promises as fs } from 'fs';
import path from 'path';
import dynamic from 'next/dynamic';

export default function Contact({ source }) {
  const MapWithNoSSR = dynamic(() => import('../components/map.js'), {
    ssr: false,
  });
  return <Layout>
    <article className="prose py-10 px-5 bg-white rounded-md shadow-lg w-3/4 md:max-w-2xl">
      <MDXRemote {...source} />
      <div>
        <MapWithNoSSR/>
      </div>
    </article>
  </Layout>;
}

export async function getStaticProps() {
  const postFile = path.join(process.cwd(), 'database', 'posts', 'contact.md');
  const data = await fs.readFile(postFile, 'utf-8');
  const markdownSource = await serialize(data);
  return {
    props: {
      source: markdownSource,
    },
  };
}
